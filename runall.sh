#!/bin/bash
regex="*.c"
for file in *
do
    if [[ ${file} =~ program[0-9]{1,}.c ]]
    then
        printf "\n\n\n------------------------------${file}------------------------------\n"
        cat $file
        echo "----------OUTPUT----------"
        gcc -o out $file && ./out
    fi
done
